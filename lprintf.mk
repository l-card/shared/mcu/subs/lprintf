# makefile для включения в проект.
#
# на входе принимает параметры:
#  TARGET_CPU (или может быть переопределен через LPRINTF_TARGET) - определяет,
#              какой порт будет использоваться. Должен быть задан один
#              из поддерживаемых портов.
#
# устанавливает следующие параметры:
#  LPRINTF_SRC      - исходные файлы на C
#  LPRINTF_INC_DIRS - директории с заголовками, которые должны быть добавлены
#                     в список директорий для поиска заголовков проекта

LPRINTF_MAKEFILE = $(abspath $(lastword $(MAKEFILE_LIST)))
LPRINTF_DIR := $(dir $(LPRINTF_MAKEFILE))

LPRINTF_TARGET ?= $(TARGET_CPU)

LPRINTF_SRC := $(LPRINTF_DIR)/lprintf.c

LPRINTF_INC_DIRS := $(LPRINTF_DIR)


LPRINTF_PORT_SET = 0
#append port file to sources
ifeq ($(LPRINTF_TARGET), lpc17xx)
    LPRINTF_SRC += $(LPRINTF_DIR)/lpc/lpc17xx_uart.c
    LPRINTF_PORT_SET = 1
else ifeq ($(LPRINTF_TARGET), lpc43xx)
    LPRINTF_SRC += $(LPRINTF_DIR)/lpc/lpc43xx_uart.c
    LPRINTF_PORT_SET = 1
else ifeq ($(LPRINTF_TARGET), lpc11u6x)
    LPRINTF_SRC += $(LPRINTF_DIR)/lpc/lpc11u6x_uart.c
    LPRINTF_PORT_SET = 1
else ifeq ($(LPRINTF_TARGET), bf609)
    LPRINTF_SRC += $(LPRINTF_DIR)/bfin/bf609_uart.c
    LPRINTF_PORT_SET = 1
else ifeq ($(LPRINTF_TARGET), stm32_uart_v1)
    LPRINTF_SRC += $(LPRINTF_DIR)/stm/stm32_uart_v1.c
    LPRINTF_PORT_SET = 1
else ifeq ($(LPRINTF_TARGET), stm32_uart_v2)
    LPRINTF_SRC += $(LPRINTF_DIR)/stm/stm32_uart_v2.c
    LPRINTF_PORT_SET = 1
else ifeq ($(LPRINTF_TARGET), hpm_uart)
    LPRINTF_SRC += $(LPRINTF_DIR)/hpm/hpm_uart.c
    LPRINTF_PORT_SET = 1
else ifeq ($(LPRINTF_TARGET), imx8)
    LPRINTF_SRC += $(LPRINTF_DIR)/imx/imx8_uart.c
    LPRINTF_PORT_SET = 1
else ifeq ($(LPRINTF_TARGET), atmega)
    LPRINTF_SRC += $(LPRINTF_DIR)/avr/atmega_uart.c
    LPRINTF_PORT_SET = 1
else
    $(error lprintf err: port architecture should be specified in LPRINTF_TARGET or TARGET_CPU variable)
endif

