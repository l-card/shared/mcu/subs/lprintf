#ifndef LPRINTF_CONFIG_H
#define LPRINTF_CONFIG_H

/* номер порта UART контроллера */
#define LPRINTF_UART_NUM       1
/* признак, что используется LOW-POWER UART (свои номера) */
#define LPRINTF_UART_LP        0
/* конфигурация пина, используемого для передачи данных по UART */
#define LPRINTF_UART_TX_PIN    CHIP_PIN_PB14_USART1_TX

#endif // LPRINTF_CONFIG_H
