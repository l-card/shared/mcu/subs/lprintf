#ifndef LPRINTF_H__
#define LPRINTF_H__

#include <stdint.h>

typedef enum {
    LPRINTF_UART_PARITY_NONE = 0,
    LPRINTF_UART_PARITY_ODD  = 1,
    LPRINTF_UART_PARITY_EVEN = 2
} t_lprintf_uart_parity;

typedef enum {
    LPRINTF_ERR_UART_INVALID_BITLEN = -1,
    LPRINTF_ERR_UART_INVALID_PARITY = -2,
    LPRINTF_ERR_UART_INVALID_BAUDRATE = -3
} t_lprintf_errs;


int lprintf_uart_init(uint32_t baud, uint8_t databits, t_lprintf_uart_parity parity);

void lprintf_close(void);
void lprintf_putchar(char c);
int lprintf_uart_busy(void);

int lprintf(const char *fmt, ...);

#endif
